# https://www.digitalocean.com/community/tutorials/how-to-set-up-apache-virtual-hosts-on-ubuntu-16-04
# vytvori novy adresar pro zdrojove soubory projektu

bold=$(tput bold)
normal=$(tput sgr0)
folder="$HOME/projekty/$1"

if [ "$2" == "nette" ]
then 
	documentRoot="$folder/www" 
else 
	documentRoot=$folder
fi;

if [ "$3" != "" ]
then 
	domaintld="$3" 
else 
	domaintld="localhost"
fi;

mkdir -p $folder

# na konec hosts souboru prida pristup pro novy projekt
echo "127.0.0.1	$1.$domaintld" | sudo tee --append /etc/hosts > /dev/null

# vygeneruje novy konfiguracni soubor pro projekt
echo "
NameVirtualHost *:80
NameVirtualHost *:443

<VirtualHost *:80>
    DocumentRoot $documentRoot
    ServerName $1.$domaintld
    <Directory $documentRoot>
        AllowOverride All
        Require local
    </Directory>
</VirtualHost>

<VirtualHost *:443>
    DocumentRoot $documentRoot
    ServerName $1.$domaintld
    SSLEngine on
    SSLCertificateFile	/etc/ssl/certs/ssl-cert-snakeoil.pem
    SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key

    BrowserMatch "MSIE [2-6]" \
        nokeepalive ssl-unclean-shutdown \
        downgrade-1.0 force-response-1.0
    BrowserMatch "MSIE [17-9]" ssl-unclean-shutdown

    <Directory $documentRoot>
        AllowOverride All
        Require local
    </Directory>
</VirtualHost>
" | sudo tee --append /etc/apache2/sites-available/$1.$domaintld.conf > /dev/null

# povoli novy konfiguracni soubor
sudo a2ensite $1.$domaintld.conf

# reload sluzby apache
sudo service apache2 reload

echo "Projekt byl vytvoren na domene ${bold}$1.$domaintld${normal} ve slozce ${bold}$folder${normal}"


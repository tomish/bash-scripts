if [ "$1" != "" ]
then 
	phpVersion=$1 
else 
	phpVersion="7.1"
fi;

sudo a2dismod php5.6
sudo a2dismod php7.0
sudo a2dismod php7.1
sudo a2dismod php7.2
sudo a2enmod php$phpVersion
sudo service apache2 restart
sudo ln -sfn /usr/bin/php$phpVersion /etc/alternatives/php
echo "PHP nastaveno na verzi $phpVersion"
